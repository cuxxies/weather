package id.cuxxie.androidweather.Model.Parser;

import android.support.annotation.NonNull;
import android.support.v4.util.Pair;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import id.cuxxie.androidweather.Model.DBModel.Weather;

import static java.lang.System.in;

/**
 * Created by hendr on 5/20/2017.
 */

public class WeatherParser {
    public static Pair<Integer,String> getWeatherDetailsFromOpenWeatherJsonTemperature(JsonObject jsonObject)
    {
        JsonArray weatherArray = jsonObject.get("weather").getAsJsonArray();
        if(weatherArray.size() > 0) {
            JsonObject weatherObject = weatherArray.get(0).getAsJsonObject();
            String weatherText = weatherObject.get("main").getAsString();
            Integer weatherStatusId = weatherObject.get("id").getAsInt();
            return new Pair<>(weatherStatusId,weatherText);
        }
        return null;
    }

    public static Weather parseOpenWeatherJsonTemperatureToWeather(JsonObject jsonObject,int cityId) {
        JsonObject temperatureJsonObject = jsonObject.get("main").getAsJsonObject();
        double min = temperatureJsonObject.get("temp_min").getAsDouble();
        double max = temperatureJsonObject.get("temp_max").getAsDouble();
        long date = jsonObject.get("dt").getAsLong();
        long reqDate = System.currentTimeMillis();
        Pair<Integer, String> weatherDetails = getWeatherDetailsFromOpenWeatherJsonTemperature(jsonObject);
        Weather weather = new Weather(min,max,date,reqDate,cityId,weatherDetails.second,weatherDetails.first);
        return weather;
    }

    public static int getCityIdFromOpenWeatherJson(JsonObject jsonObject)
    {
        int cityId;
        JsonObject cityJsonObject = jsonObject.get("city").getAsJsonObject();
        cityId = 0;
        //cityJsonObject.get("geoname_id").getAsInt();
        return cityId;
    }

    public static ArrayList<Weather> parseOpenWeatherJsonToWeatherList(JsonObject jsonObject) {
        ArrayList<Weather> weatherList = new ArrayList<Weather>();
        int cityId = getCityIdFromOpenWeatherJson(jsonObject);
        JsonArray temperatureArray = jsonObject.get("list").getAsJsonArray();

        for(Object obj: temperatureArray){
            if ( obj instanceof JsonObject ) {
                JsonObject objJson = (JsonObject)obj;
                weatherList.add(parseOpenWeatherJsonTemperatureToWeather(objJson,cityId));
            }
        }
        return weatherList;
    }
}
