package id.cuxxie.androidweather.Model.ObservableModel;

import android.databinding.ObservableArrayList;

import id.cuxxie.androidweather.Model.DBModel.Weather;

/**
 * Created by hendr on 5/21/2017.
 */

public class ObservableWeatherList {
    ObservableArrayList<Weather> list = new ObservableArrayList<>();
    private int total;

    public void add(Weather weather)
    {
//        ObservableWeather observableWeather = new ObservableWeather();
//        observableWeather.setValuesFromPOJO(weather);
        list.add(weather);
        total = list.size();
    }

//    public void add(ObservableWeather observableWeather)
//    {
//        list.add(observableWeather);
//        total = list.size();
//    }

    public void remove() {
        if (!list.isEmpty()) {
            list.remove(0);
        }
    }
}
