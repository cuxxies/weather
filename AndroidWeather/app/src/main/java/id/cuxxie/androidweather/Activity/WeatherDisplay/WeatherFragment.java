package id.cuxxie.androidweather.Activity.WeatherDisplay;

import android.content.Context;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import id.cuxxie.androidweather.Model.APIHandler.APIOperationInterfaces;
import id.cuxxie.androidweather.Model.ObservableModel.ObservableWeather;
import id.cuxxie.androidweather.Model.ObservableModel.ObservableWeatherList;
import id.cuxxie.androidweather.databinding.FragmentWeatherBinding;

public class WeatherFragment extends Fragment implements WeatherDisplayContract.View {

    FragmentWeatherBinding binding;



    WeatherDisplayPresenter weatherDisplayPresenter;

    public ObservableWeatherList getObservableWeather() {
        return observableWeatherList;
    }

    ObservableWeatherList observableWeatherList;
    public WeatherFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        observableWeatherList = new ObservableWeatherList();
        weatherDisplayPresenter = new WeatherDisplayPresenter(this, observableWeatherList);
        binding = FragmentWeatherBinding.inflate(inflater, container, false);
        binding.setPresenter(weatherDisplayPresenter);
        //binding.setWeather(observableWeather);
        return binding.getRoot();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public APIOperationInterfaces getWeatherAPICallerInterface() {
        return weatherDisplayPresenter;
    }

    @Override
    public void showData(ObservableWeather temperatureData) {

    }
}
