package id.cuxxie.androidweather.Model.APIHandler;

import android.content.Context;
import android.support.v4.util.Pair;

import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.Response;
import com.koushikdutta.ion.future.ResponseFuture;

import org.json.JSONArray;

import java.util.List;

/**
 * Created by hendr on 5/19/2017.
 */

public class APICaller {
    private static APICaller instance;
    private String url;
    private String API_KEY = "53e240fa6b7a8ca9c3b19891dfff136c";
    private String HOST = "http://api.openweathermap.org/data/2.5/forecast?id=";
    public static APICaller getInstance() {
        if (instance == null)
            instance = new APICaller();
        return instance;
    }

    public void callWeatherForecast(Context context ,final APIOperationInterfaces callback, int cityId, boolean isTest) {
        if(!isTest)
        {
            url = HOST+String.valueOf(cityId)+"&appid="+API_KEY;
        }
        else
        {
            url = "http://samples.openweathermap.org/data/2.5/forecast?id=524901&appid=b1b15e88fa797225412429c1c50c122a1";
        }
        Ion.with(context).load(url).asJsonObject().withResponse().setCallback(new FutureCallback<Response<JsonObject>>() {
            @Override
            public void onCompleted(Exception e, Response<JsonObject> result) {
                if(e != null) {
                    callback.onAPICallError(e,result.getHeaders().code(),result.getHeaders().message());
                }
                else if (result != null) {
                    String urlCalled = result.getRequest().getUri().toString();
                    if (result.getHeaders().code() == 200) {
                        callback.onAPICallCompleted(result.getResult());
                    } else {
                        callback.onAPICallError(e,result.getHeaders().code(),result.getHeaders().message());
                    }
                }
            }
        });
    }
}
