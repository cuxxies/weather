package id.cuxxie.androidweather.Model.DBHandler;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

/**
 * Created by hendr on 5/20/2017.
 */

public class WeatherDatabaseHelper extends SQLiteAssetHelper {

    private static final String DATABASE_NAME = "weather.db";
    private static final int DATABASE_VERSION = 1;

    public WeatherDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public Cursor getAllCountries() {
        SQLiteDatabase db = getReadableDatabase();
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        String[] sqlSelect = {"code", "Name"};
        String sqlTables = "countries";
        qb.setTables(sqlTables);
        Cursor c = qb.query(db, sqlSelect, null, null,
                null, null, null);
        c.moveToFirst();
        return c;
    }

    public Cursor getCountriesWithName(String name)
    {
        SQLiteDatabase db = getReadableDatabase();
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        String[] sqlSelect = {"code", "Name"};
        String whereClause = "Name LIKE ?";
        String[] whereArg = {name+"%"};
        String sqlTables = "countries";
        qb.setTables(sqlTables);
        Cursor c = qb.query(db, sqlSelect, whereClause, whereArg,
                null, null, null);
        c.moveToFirst();
        return c;
    }

    public Cursor getCitiesWithCountryAndName(String countryCode, String cityName, int cityId)
    {
        SQLiteDatabase db = getReadableDatabase();
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        String[] sqlSelect = {"id", "name", "lat", "longi", "countryCode"};
        String whereClause = "countryCode = ? AND name LIKE ?";
        String[] whereArg = {countryCode,cityName+"%"};
        if(cityId != 0) {
            whereClause = whereClause + "AND id = ?";
            String[] whereArgWithCityId = {countryCode,cityName+"%",String.valueOf(cityId)};
            whereArg = whereArgWithCityId;
        }
        String sqlTables = "cities";
        String sortBy = "name ASC";
        qb.setTables(sqlTables);
        Cursor c = qb.query(db, sqlSelect, whereClause, whereArg,
                null, null, sortBy);
        c.moveToFirst();
        return c;
    }

    public Cursor getCitiesWithCountry(String countryCode)
    {
        SQLiteDatabase db = getReadableDatabase();
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        String[] sqlSelect = {"id", "name", "lat", "longi", "countryCode"};
        String whereClause = "countryCode = ?";
        String[] whereArg = {countryCode};
        String sqlTables = "cities";
        String sortBy = "name ASC";
        qb.setTables(sqlTables);
        Cursor c = qb.query(db, sqlSelect, whereClause, whereArg,
                null, null, sortBy);
        c.moveToFirst();
        return c;
    }
}