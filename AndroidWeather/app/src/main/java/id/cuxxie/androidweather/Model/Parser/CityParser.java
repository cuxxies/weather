package id.cuxxie.androidweather.Model.Parser;

import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;

import id.cuxxie.androidweather.Model.DBHandler.WeatherDatabaseHelper;
import id.cuxxie.androidweather.Model.DBModel.City;

/**
 * Created by hendr on 5/20/2017.
 */

public class CityParser {
    static int cityIdDBIndex = 0;
    static int cityNameDBIndex = 1;
    static int cityLatDBIndex = 2;
    static int cityLongiDBIndex = 3;
    static int cityCountryCodeDBIndex = 4;

    public static ArrayList<City> getCitiesWithCountryFromDB(Context context, String countryCode)
    {
        WeatherDatabaseHelper db = new WeatherDatabaseHelper(context);
        Cursor cursor = db.getCitiesWithCountry(countryCode);
        return processCursorToCityList(cursor);
    }

    public static ArrayList<City> getCitiesWithCountryAndNameFromDB(Context context, String countryCode, String cityName)
    {
        WeatherDatabaseHelper db = new WeatherDatabaseHelper(context);
        Cursor cursor = db.getCitiesWithCountryAndName(countryCode,cityName,0);
        return processCursorToCityList(cursor);
    }

    private static ArrayList<City> processCursorToCityList(Cursor cursor)
    {
        ArrayList cityList = new ArrayList<City>();
        if(cursor.getCount() > 0) {
            do {
                cityList.add(cityFromCursor(cursor));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return cityList;
    }

    public static City getCityFromDB(Context context, String countryCode, String cityName, int cityId)
    {
        WeatherDatabaseHelper db = new WeatherDatabaseHelper(context);
        Cursor cursor = db.getCitiesWithCountryAndName(countryCode,cityName,cityId);
        return cityFromCursor(cursor);
    }

    public static City cityFromCursor(Cursor cursor)
    {
        if(cursor.getCount() == 0)
            return null;

        City city = new City(cursor.getInt(cityIdDBIndex),
                             cursor.getString(cityNameDBIndex),
                             cursor.getDouble(cityLatDBIndex),
                             cursor.getDouble(cityLongiDBIndex),
                             cursor.getString(cityCountryCodeDBIndex));
        ;
        return city;
    }
}
