package id.cuxxie.androidweather.Model.APIHandler;

import com.google.gson.JsonObject;

import org.json.JSONArray;

/**
 * Created by hendr on 5/19/2017.
 */

public interface APIOperationInterfaces {
    void onAPICallCompleted(JsonObject result);
    void onAPICallError(Exception error, int code, String message);
}
