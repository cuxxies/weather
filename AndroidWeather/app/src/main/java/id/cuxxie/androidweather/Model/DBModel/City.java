package id.cuxxie.androidweather.Model.DBModel;

import ir.mirrajabi.searchdialog.core.Searchable;

/**
 * Created by hendr on 5/19/2017.
 */

public class City implements Searchable{
    int id;
    String name;
    double lat;
    double longi;
    String countryCode;

    public City(int id, String name, double lat, double longi, String countryCode) {
        this.id = id;
        this.name = name;
        this.lat = lat;
        this.longi = longi;
        this.countryCode = countryCode;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLongi() {
        return longi;
    }

    public void setLongi(double longi) {
        this.longi = longi;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String toString()
    {
        return this.name;
    }

    @Override
    public String getTitle() {
        return this.name;
    }
}
