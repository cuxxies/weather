package id.cuxxie.androidweather.ViewModel;

/**
 * Created by hendr on 5/20/2017.
 */

public interface ViewModelInterface {
    public void onCreate();
    public void onPause();
    public void onResume();
    public void onDestroy();
}
