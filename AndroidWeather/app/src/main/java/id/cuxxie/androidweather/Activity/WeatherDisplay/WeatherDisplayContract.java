package id.cuxxie.androidweather.Activity.WeatherDisplay;

import id.cuxxie.androidweather.Model.ObservableModel.ObservableWeather;

/**
 * Created by hendr on 5/21/2017.
 */

public interface WeatherDisplayContract {
    public interface Presenter {
        void onShowData(ObservableWeather temperatureData);
    }

    public interface View {
        void showData(ObservableWeather temperatureData);
    }

}