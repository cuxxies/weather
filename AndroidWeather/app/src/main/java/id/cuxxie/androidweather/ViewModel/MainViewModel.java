package id.cuxxie.androidweather.ViewModel;

import android.content.Context;
import android.content.SharedPreferences;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.JsonObject;

import java.util.ArrayList;

import id.cuxxie.androidweather.Activity.MainActivity;
import id.cuxxie.androidweather.Activity.WeatherDisplay.WeatherFragment;
import id.cuxxie.androidweather.Model.APIHandler.APICaller;
import id.cuxxie.androidweather.Model.APIHandler.APIOperationInterfaces;
import id.cuxxie.androidweather.Model.DBModel.City;
import id.cuxxie.androidweather.Model.DBModel.Weather;
import id.cuxxie.androidweather.Model.Parser.CityParser;
import id.cuxxie.androidweather.Model.Parser.WeatherParser;
import ir.mirrajabi.searchdialog.SimpleSearchDialogCompat;
import ir.mirrajabi.searchdialog.core.BaseSearchDialogCompat;
import ir.mirrajabi.searchdialog.core.SearchResultListener;

/**
 * Created by hendr on 5/20/2017.
 */
public class MainViewModel implements ViewModelInterface {
    static String APP_PREFS_KEY = "weatherAppPrefs";
    static String SELECTED_COUNTRY_PREFS_KEY = "selectedCountryCode";
    static String SELECTED_COUNTRY_NAME_PREFS_KEY = "selectedCountryName";
    static String SELECTED_CITY_NAME_PREFS_KEY = "selectedCityName";
    static String SELECTED_CITY_ID_PREFS_KEY = "selectedCityId";

    public WeatherFragment weatherFragment;
    String selectedCountry = "";


    String selectedCountryName = "";
    City selectedCity;
    Context context;
    ArrayAdapter<City> cityItemsAdapter;
    ArrayList<City> cityArrayList;

    public MainViewModel(Context context) {
        this.context = context;
        cityArrayList = new ArrayList<City>();

        //load previously saved settings
        SharedPreferences settings =
                context.getSharedPreferences(APP_PREFS_KEY, 0);
        selectedCountry = settings.getString(SELECTED_COUNTRY_PREFS_KEY,"");
        selectedCountryName = settings.getString(SELECTED_COUNTRY_NAME_PREFS_KEY,"");
        int cityId = settings.getInt(SELECTED_CITY_ID_PREFS_KEY, 0);
        String cityName = settings.getString(SELECTED_CITY_NAME_PREFS_KEY,"");
        if(cityId != 0)
        {
           selectedCity = CityParser.getCityFromDB(context,selectedCountry,cityName,cityId);
        }
    }

    public void selectCountry(String countryCode, String countryName) {
        selectedCountry = countryCode;
        selectedCountryName = countryName;
        SharedPreferences settings =
                context.getSharedPreferences(APP_PREFS_KEY, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(SELECTED_COUNTRY_PREFS_KEY,countryCode);
        editor.putString(SELECTED_COUNTRY_NAME_PREFS_KEY,countryName);
        editor.commit();
    }


    public void selectCity(City city) {
        selectedCity = city;
        SharedPreferences settings =
                context.getSharedPreferences(APP_PREFS_KEY, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(SELECTED_CITY_NAME_PREFS_KEY,city.getName());
        editor.putInt(SELECTED_CITY_ID_PREFS_KEY,city.getId());
        editor.commit();
    }

    public String getSelectedCityName(){
        if(selectedCity == null) {
            return "";
        }
        return selectedCity.getName();
    }

    public String getSelectedCountryName() {
        return selectedCountryName;
    }

    public int getSelectedCityId() {
        if(selectedCity == null) {
            return 0;
        }
        return selectedCity.getId();
    }



    public String getSelectedCountry() {
        return selectedCountry;
    }

    public ArrayList<City> loadAllCityArrayFromDB(){
        return CityParser.getCitiesWithCountryFromDB(context,selectedCountry);
    }

    private ArrayList<City> loadCityArrayFromDB(String cityName) {
        return CityParser.getCitiesWithCountryAndNameFromDB(context, selectedCountry, cityName);
    }

    @Override
    public void onCreate() {
    }

    @Override
    public void onPause() {
    }

    @Override
    public void onResume() {
    }

    @Override
    public void onDestroy() {
    }

    public void onTextChange(String textChange) {
        cityArrayList = loadCityArrayFromDB(textChange);
        cityItemsAdapter.clear();
        cityItemsAdapter.addAll(cityArrayList);
        //loadCityArrayFromDB(textChange);

    }

    public void setupListViewAdapter(ListView listView, int layoutResId) {
        cityItemsAdapter =
                new ArrayAdapter<City>(context, layoutResId, cityArrayList);
        listView.setAdapter(cityItemsAdapter);
    }

    public void loadWeatherDataFromAPI()
    {
        APICaller.getInstance().callWeatherForecast(context,weatherFragment.getWeatherAPICallerInterface(),getSelectedCityId(),false);
    }
}
