package id.cuxxie.androidweather.Model.ObservableModel;

import android.databinding.ObservableField;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Date;

import id.cuxxie.androidweather.Model.DBModel.Weather;

/**
 * Created by hendr on 5/20/2017.
 */

public class ObservableWeather {
    public final ObservableField<String> tempMin = new ObservableField<>();
    public final ObservableField<String> tempMax = new ObservableField<>();
    public final ObservableField<String> date = new ObservableField<>();
    public final ObservableField<String> requested_date = new ObservableField<>();
    public final ObservableField<String> cityId = new ObservableField<>();
    public final ObservableField<String> weatherText = new ObservableField<>();
    public final ObservableField<String> weatherStatusId = new ObservableField<>();

    public void setValuesFromPOJO(Weather weather)
    {
        tempMin.set(String.valueOf(weather.getTempMin()));
        tempMax.set(String.valueOf(weather.getTempMax()));
        Date myDate = new Date(weather.getDate());
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        date.set(simpleDateFormat.format(myDate));
        requested_date.set(String.valueOf(weather.getRequested_date()));
        cityId.set(String.valueOf(weather.getCityId()));
        weatherText.set(weather.getWeatherText());
        weatherStatusId.set(String.valueOf(weather.getWeatherStatusId()));

        //Log.i("Obsrvbl Weather Update",tempMin.get()+" "+tempMax.get()+" "+date.get());
    }
}
