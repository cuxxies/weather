package id.cuxxie.androidweather.Activity;

import android.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.mukesh.countrypicker.CountryPicker;
import com.mukesh.countrypicker.CountryPickerListener;

import id.cuxxie.androidweather.Activity.WeatherDisplay.WeatherFragment;
import id.cuxxie.androidweather.Model.DBModel.City;
import id.cuxxie.androidweather.R;
import id.cuxxie.androidweather.Utility.CustomSearchFilter;
import id.cuxxie.androidweather.ViewModel.MainViewModel;
import ir.mirrajabi.searchdialog.SimpleSearchDialogCompat;
import ir.mirrajabi.searchdialog.core.BaseSearchDialogCompat;
import ir.mirrajabi.searchdialog.core.SearchResultListener;

public class MainActivity extends AppCompatActivity {
    MainViewModel mainViewModel;
    Button countryButton;
    Button cityButton;

    WeatherFragment weatherFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainViewModel = new MainViewModel(getApplicationContext());
        setContentView(R.layout.activity_main);
        cityButton = (Button)findViewById(R.id.mainActivity_cityButton);
        cityButton.setText(mainViewModel.getSelectedCityName());
        countryButton = (Button)findViewById(R.id.mainActivity_countryButton);
        countryButton.setText(mainViewModel.getSelectedCountryName());
        setupWeatherFragment();
    }


    void setupWeatherFragment()
    {
        weatherFragment = new WeatherFragment();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.mainActivity_weatherContainer, weatherFragment);
        transaction.commit();
        mainViewModel.weatherFragment = weatherFragment;
    }

    void onCountrySelected(String name, String code)
    {
        mainViewModel.selectCountry(code,name);
        countryButton.setText(mainViewModel.getSelectedCountryName());
    }

    void onCitySelected(City city)
    {
        mainViewModel.selectCity(city);
        cityButton.setText(mainViewModel.getSelectedCityName());
        mainViewModel.loadWeatherDataFromAPI();
    }
    void displayCityDialog(View view)
    {
        SimpleSearchDialogCompat searchDialogCompat =
                new SimpleSearchDialogCompat(MainActivity.this, "Search...",
                        "Which City are you looking for...?", null, mainViewModel.loadAllCityArrayFromDB(),new SearchResultListener<City>() {
                    @Override
                    public void onSelected(BaseSearchDialogCompat dialog,
                                           City item, int position) {
                        onCitySelected(item);
                        dialog.dismiss();
                    }});
        searchDialogCompat.show();
        searchDialogCompat.setFilter(new CustomSearchFilter<City>(searchDialogCompat.getItems(),searchDialogCompat.getFilterResultListener()));
    }

    void displayCountryDialog(View view)
    {
        final CountryPicker picker = CountryPicker.newInstance("Select Country");  // dialog title
        picker.setListener(new CountryPickerListener() {
            @Override
            public void onSelectCountry(String name, String code, String dialCode, int flagDrawableResID) {
                onCountrySelected(name,code);
                picker.dismiss();
            }
        });
        picker.show(getSupportFragmentManager(), "COUNTRY_PICKER");
    }


}
