package id.cuxxie.androidweather.Model.DBModel;

/**
 * Created by hendr on 5/19/2017.
 */

/*
{
   "coord":{
      "lon":145.77,
      "lat":-16.92
   },
   "weather":[
      {
         "id":802,
         "main":"Clouds",
         "description":"scattered clouds",
         "icon":"03n"
      }
   ],
   "base":"stations",
   "main":{
      "temp":300.15,
      "pressure":1007,
      "humidity":74,
      "temp_min":300.15,
      "temp_max":300.15
   },
   "visibility":10000,
   "wind":{
      "speed":3.6,
      "deg":160
   },
   "clouds":{
      "all":40
   },
   "dt":1485790200,
   "sys":{
      "type":1,
      "id":8166,
      "message":0.2064,
      "country":"AU",
      "sunrise":1485720272,
      "sunset":1485766550
   },
   "id":2172797,
   "name":"Cairns",
   "cod":200
}
*/
public class Weather {
    double tempMin;
    double tempMax;
    long date;
    long requested_date;
    int cityId;
    String weatherText;
    int weatherStatusId;

    public Weather( double tempMin, double tempMax, long date, long requested_date, int cityId, String weatherText, int weatherStatusId) {
        this.tempMin = tempMin;
        this.tempMax = tempMax;
        this.date = date;
        this.requested_date = requested_date;
        this.cityId = cityId;
        this.weatherText = weatherText;
        this.weatherStatusId = weatherStatusId;
    }

    public double getTempMin() {
        return tempMin;
    }

    public double getTempMax() {
        return tempMax;
    }

    public long getDate() {
        return date;
    }

    public long getRequested_date() {
        return requested_date;
    }

    public int getCityId() {
        return cityId;
    }

    public String getWeatherText() {
        return weatherText;
    }

    public int getWeatherStatusId() {
        return weatherStatusId;
    }

    public void setTempMin(double tempMin) {
        this.tempMin = tempMin;
    }

    public void setTempMax(double tempMax) {
        this.tempMax = tempMax;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public void setRequested_date(long requested_date) {
        this.requested_date = requested_date;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    public void setWeatherText(String weatherText) {
        this.weatherText = weatherText;
    }

    public void setWeatherStatusId(int weatherStatusId) {
        this.weatherStatusId = weatherStatusId;
    }
}
