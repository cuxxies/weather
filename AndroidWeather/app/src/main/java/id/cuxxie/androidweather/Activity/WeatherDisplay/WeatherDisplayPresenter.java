package id.cuxxie.androidweather.Activity.WeatherDisplay;


import com.google.gson.JsonObject;

import java.util.ArrayList;

import id.cuxxie.androidweather.Model.APIHandler.APIOperationInterfaces;
import id.cuxxie.androidweather.Model.DBModel.Weather;
import id.cuxxie.androidweather.Model.ObservableModel.ObservableWeather;
import id.cuxxie.androidweather.Model.ObservableModel.ObservableWeatherList;
import id.cuxxie.androidweather.Model.Parser.WeatherParser;

/**
 * Created by hendr on 5/21/2017.
 */

public class WeatherDisplayPresenter implements WeatherDisplayContract.Presenter, APIOperationInterfaces {
    private WeatherDisplayContract.View view;
    ObservableWeatherList observableWeatherList;
    public WeatherDisplayPresenter(WeatherDisplayContract.View view, ObservableWeatherList observableWeatherList) {
        this.view = view;
        this.observableWeatherList = observableWeatherList;
    }

    @Override
    public void onShowData(ObservableWeather observableWeather) {
        view.showData(observableWeather);
    }

    @Override
    public void onAPICallCompleted(JsonObject result) {
       ArrayList<Weather> weatherArrayList = WeatherParser.parseOpenWeatherJsonToWeatherList(result);
        for(int i=0;i<weatherArrayList.size();i++) {
            observableWeatherList.add(weatherArrayList.get(i));
        }
    }

    @Override
    public void onAPICallError(Exception error, int code, String message) {

    }
}