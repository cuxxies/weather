package id.cuxxie.androidweather;

import android.support.v4.util.Pair;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import junit.framework.Assert;

import org.junit.Test;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Scanner;

import id.cuxxie.androidweather.Model.DBModel.Weather;
import id.cuxxie.androidweather.Model.ObservableModel.ObservableWeather;
import id.cuxxie.androidweather.Model.Parser.WeatherParser;

/**
 * Created by hendr on 5/20/2017.
 */

public class WeatherParserUnitTest {
    @Test
    public void test_parseOpenWeatherJsonToWeatherList()
    {
        InputStream is = getClass().getClassLoader().getResourceAsStream("jsonDailyResult.json");
        Scanner s = new Scanner(is).useDelimiter("\\A");
        String result = s.hasNext() ? s.next() : "";
        JsonParser jsonParser = new JsonParser();
        JsonObject jo = (JsonObject)jsonParser.parse(result);
        JsonArray list = jo.get("list").getAsJsonArray();
        JsonObject weatherListFirstObject = list.get(0).getAsJsonObject();

        //Test cityId
        //Assert.assertEquals(524901,WeatherParser.getCityIdFromOpenWeatherJson(jo));

        //Test weather details
        Pair<Integer,String> pair = new Pair<Integer,String>(800,"Clear");
        Assert.assertEquals(pair.first,WeatherParser.getWeatherDetailsFromOpenWeatherJsonTemperature(weatherListFirstObject).first);
        Assert.assertEquals(pair.second,WeatherParser.getWeatherDetailsFromOpenWeatherJsonTemperature(weatherListFirstObject).second);

        //Test single object parser
        //Assert.assertEquals(524901,WeatherParser.parseOpenWeatherJsonTemperatureToWeather(weatherListFirstObject,524901).getCityId());
        Assert.assertEquals(800,WeatherParser.parseOpenWeatherJsonTemperatureToWeather(weatherListFirstObject,524901).getWeatherStatusId());
        Assert.assertEquals("Clear",WeatherParser.parseOpenWeatherJsonTemperatureToWeather(weatherListFirstObject,524901).getWeatherText());
        Assert.assertEquals(259.086,WeatherParser.parseOpenWeatherJsonTemperatureToWeather(weatherListFirstObject,524901).getTempMin());
        Assert.assertEquals(261.45,WeatherParser.parseOpenWeatherJsonTemperatureToWeather(weatherListFirstObject,524901).getTempMax());

        //Test multiObjectparser
        ArrayList<Weather> weatherArrayList = WeatherParser.parseOpenWeatherJsonToWeatherList(jo);
        Weather singleWeather = weatherArrayList.get(0);
        //Assert.assertEquals(524901,singleWeather.getCityId());
        Assert.assertEquals(800,singleWeather.getWeatherStatusId());
        Assert.assertEquals("Clear",singleWeather.getWeatherText());
        Assert.assertEquals(259.086,singleWeather.getTempMin());
        Assert.assertEquals(261.45,singleWeather.getTempMax());

        //test weatherObservable
        ObservableWeather observableWeather = new ObservableWeather();
        observableWeather.setValuesFromPOJO(singleWeather);
//        Assert.assertEquals(observableWeather.cityId.get().intValue(),singleWeather.getCityId());
//        Assert.assertEquals(observableWeather.weatherStatusId.get().intValue(),singleWeather.getWeatherStatusId());
//        Assert.assertEquals(observableWeather.weatherText.get(),singleWeather.getWeatherText());
//        Assert.assertEquals(observableWeather.date.get().longValue(),singleWeather.getDate());
//        Assert.assertEquals(observableWeather.requested_date.get().longValue(),singleWeather.getRequested_date());
//        Assert.assertEquals(observableWeather.tempMax.get(),singleWeather.getTempMax());
//        Assert.assertEquals(observableWeather.tempMin.get(),singleWeather.getTempMin());
//        Assert.assertEquals(observableWeather.dayTemp.get(),singleWeather.getDayTemp());
//        Assert.assertEquals(observableWeather.eveTemp.get(),singleWeather.getEveTemp());
//        Assert.assertEquals(observableWeather.mornTemp.get(),singleWeather.getMornTemp());
//        Assert.assertEquals(observableWeather.nightTemp.get(),singleWeather.getNightTemp());
    }
}
