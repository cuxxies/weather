package id.cuxxie.androidweather;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.widget.ArrayAdapter;

import com.google.gson.JsonObject;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.mockito.exceptions.base.MockitoAssertionError;
import org.mockito.exceptions.verification.TooLittleActualInvocations;
import org.mockito.exceptions.verification.WantedButNotInvoked;
import org.mockito.exceptions.verification.junit.ArgumentsAreDifferent;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;

import id.cuxxie.androidweather.Model.APIHandler.APICaller;
import id.cuxxie.androidweather.Model.APIHandler.APIOperationInterfaces;
import id.cuxxie.androidweather.Model.DBModel.Weather;
import id.cuxxie.androidweather.Model.Parser.WeatherParser;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static org.junit.Assert.*;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(MockitoJUnitRunner.class)
public class ExampleInstrumentedTest {
    @Before
    public void setUp()
    {
        System.setProperty(
                "dexmaker.dexcache",
                getInstrumentation().getTargetContext().getCacheDir().getPath());
    }

    @Test
    public void useAppContext() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("id.cuxxie.androidweather", appContext.getPackageName());
    }

    @Test
    public void correctURLAPICallTest() throws Exception {
        APIOperationInterfaces callback = Mockito.mock(APIOperationInterfaces.class);
        APICaller.getInstance().callWeatherForecast(getInstrumentation().getTargetContext().getApplicationContext(),callback,0,true);
        ArgumentCaptor<JsonObject> captor = ArgumentCaptor.forClass( JsonObject.class );
        Mockito.verify(callback,Mockito.after(5000)).onAPICallCompleted(captor.capture());
        JsonObject result = captor.getValue();
        validateCalledAPIJson(result);
    }

    public void validateCalledAPIJson(JsonObject jsonObject)
    {
        String resultCode = jsonObject.get("cod").getAsString();
        assertEquals(resultCode,"200");

        ArrayList<Weather> weatherArrayList = WeatherParser.parseOpenWeatherJsonToWeatherList(jsonObject);
        Assert.assertEquals(weatherArrayList.get(0).getTempMin(),259.086);
    }
}
