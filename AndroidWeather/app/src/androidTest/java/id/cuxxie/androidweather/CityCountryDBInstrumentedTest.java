package id.cuxxie.androidweather;

import android.content.Context;
import android.database.Cursor;
import android.support.test.runner.AndroidJUnit4;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;

import id.cuxxie.androidweather.Model.DBHandler.WeatherDatabaseHelper;
import id.cuxxie.androidweather.Model.DBModel.City;
import id.cuxxie.androidweather.Model.Parser.CityParser;

import static android.support.test.InstrumentationRegistry.getInstrumentation;

/**
 * Created by hendr on 5/20/2017.
 */
@RunWith(AndroidJUnit4.class)
public class CityCountryDBInstrumentedTest {
    Context context;
    @Before
    public void setUp()
    {
       context = getInstrumentation().getTargetContext().getApplicationContext();
    }

    @Test
    public void testGetCitiesFromDB() throws Exception{
        WeatherDatabaseHelper dbHelper = new WeatherDatabaseHelper(context);
        Cursor citiesCursor = dbHelper.getCitiesWithCountry("ID");
//        int cityIdIndex = citiesCursor.getColumnIndexOrThrow("id");
        int cityNameIndex = citiesCursor.getColumnIndexOrThrow("name");
//        int cityLatIndex = citiesCursor.getColumnIndexOrThrow("lat");
//        int cityLongiIndex = citiesCursor.getColumnIndexOrThrow("longi");
//        int cityCountryCodeIndex = citiesCursor.getColumnIndexOrThrow("countryCode");

        Assert.assertEquals("Abepura",citiesCursor.getString(cityNameIndex));

        citiesCursor = dbHelper.getCitiesWithCountryAndName("ID","Sem",0);
        Assert.assertEquals("Semarang",citiesCursor.getString(cityNameIndex));

        ArrayList<City> cities = CityParser.getCitiesWithCountryAndNameFromDB(context,"ID","Sem");
        Assert.assertEquals("Semarang",cities.get(0).getName());
    }

    @Test
    public void testGetCountriesFromDB() throws Exception {
        WeatherDatabaseHelper dbHelper = new WeatherDatabaseHelper(context);
        Cursor countriesCursor = dbHelper.getAllCountries();
        Assert.assertEquals(250,countriesCursor.getCount());
        int countryCodeIndex = countriesCursor.getColumnIndexOrThrow("code");
        int countryNameIndex = countriesCursor.getColumnIndexOrThrow("Name");
        Assert.assertEquals(countriesCursor.getString(countryCodeIndex),"AF");
        Assert.assertEquals(countriesCursor.getString(countryNameIndex),"Afghanistan");

        countriesCursor = dbHelper.getCountriesWithName("Indo");
        Assert.assertEquals(1,countriesCursor.getCount());
        countryCodeIndex = countriesCursor.getColumnIndexOrThrow("code");
        countryNameIndex = countriesCursor.getColumnIndexOrThrow("Name");
        Assert.assertEquals(countriesCursor.getString(countryCodeIndex),"ID");
        Assert.assertEquals(countriesCursor.getString(countryNameIndex),"Indonesia");


    }
}
